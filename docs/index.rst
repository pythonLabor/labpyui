.. LabPyUi documentation master file, created by
   sphinx-quickstart on Sun Jun 14 09:51:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LabPyUi's documentation!
===================================

Version: .. include:: ../VERSION


.. include:: ./README.rst



Table of Contents
--------------------

.. toctree::
  :glob:
  :maxdepth: 2
  :titlesonly:

  intro
  installation/*

  development/*

  quickstart

  build_package
  todo
  changelog


.. automodule:: models

.. autoclass::
     :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
