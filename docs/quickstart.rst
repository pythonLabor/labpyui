
LabPyUi quickstart guide
===========================

To get a first impression on how simple applications can be created look at the examples in the examples section.


hello_world app
_______________

This is the bare minimum LabPyUi application.


timer app
__________

Creation of a mulitple Timer app with data logging.


temperature monitor/control app
________________________________

Here device communiction with real hardware (serial interface) is illustrated.




